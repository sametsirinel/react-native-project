const USER = {
    id: 1,
    name: "Bekir Samet",
    surname: "Şirinel"
}

const barcodeReducer = (state = USER, action) => {
    switch (action.type) {
        case "setName":
            const usr = Object.assign({}, state)
            usr.name = action.name;
            return usr;

            break;
        case "setSurname":
            const obj = Object.assign({}, state)
            obj.surname = action.surname;
            return obj;

            break;

        default:
            return state;
            break;
    }
}

export default barcodeReducer;