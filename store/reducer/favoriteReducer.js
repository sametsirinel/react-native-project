const FAVORITES = [
];

const favoriteReducer = (state = FAVORITES, action) => {
    switch (action.type) {
        case "add":
            if (state.filter(item => item.id == action.item.id).length > 0)
                return state;

            return [
                ...state,
                action.item
            ];

            break;

        case "remove":
            return state.filter(item => item.id != action.id);
            break;

        default:
            return state;
            break;
    }
}

export default favoriteReducer;