const FAVORITES = [
    {
        id: 1,
        barcode: "123213213"
    },
    {
        id: 2,
        barcode: "312321321"
    }
];

const barcodeReducer = (state = FAVORITES, action) => {
    switch (action.type) {
        case "add":
            if (state.filter(item => item.id == action.item.id).length > 0)
                return state;

            action.item.id = state.length + 1;
            return [
                ...state,
                action.item
            ];

            break;

        case "remove":
            return state.filter(item => item.id != action.id);
            break;

        default:
            return state;
            break;
    }
}

export default barcodeReducer;