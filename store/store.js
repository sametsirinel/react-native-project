import { createStore, combineReducers } from "redux";
import numberReducer from "./reducer/numberReducer";
import favoriteReducer from "./reducer/favoriteReducer";
import barcodeReducer from "./reducer/barcodeReducer";
import userReducer from "./reducer/userReducer";

const reducers = combineReducers({
    counter: numberReducer,
    favorites: favoriteReducer,
    barcodes: barcodeReducer,
    user: userReducer,
});

const store = createStore(reducers);

export default store;