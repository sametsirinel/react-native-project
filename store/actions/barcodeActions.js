const add = (item) => ({
    type: "add",
    item: item
});

const remove = (id) => ({
    type: "remove",
    id: id
});

export { add, remove };