const setName = (string) => ({
    type: "setName",
    name: string
});
const setSurname = (string) => ({
    type: "setSurname",
    surname: string
});

export { setName, setSurname };