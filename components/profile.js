import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Button, StyleSheet, TextInput } from 'react-native';
import { Layout, Input } from '@ui-kitten/components';
import { setName, setSurname } from '../store/actions/userActions';
import { ThemeContext } from '../theme/theme-context';

export default Profile = () => {

    const themeContext = React.useContext(ThemeContext);
    const dispatch = useDispatch()
    const state = useSelector(state => state)

    return (
        <Layout style={styles.container}>
            <Input
                style={styles.input}
                onChangeText={(val) => dispatch(setName(val))}
                value={state.user.name}
            />
            <Input
                style={styles.input}
                onChangeText={(val) => dispatch(setSurname(val))}
                value={state.user.surname}
            />
            <Button title='TOGGLE THEME' onPress={themeContext.toggleTheme}></Button>
        </Layout>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        padding: 60,
    }, input: {
        height: 40,
        margin: 12,
        borderWidth: 1,
        padding: 10,
    },
});
