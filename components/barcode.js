import React, { useState, useEffect } from 'react';
import { View, StyleSheet, Button, FlatList } from 'react-native';
import { BarCodeScanner } from 'expo-barcode-scanner';
import { useDispatch, useSelector } from 'react-redux';
import { add } from '../store/actions/favoriteActions';
import { Layout, Card, Text } from '@ui-kitten/components';

export default function App() {
    const [hasPermission, setHasPermission] = useState(null);
    const [camera, setCamera] = useState(false);

    const dispatch = useDispatch()
    const state = useSelector(state => state)

    useEffect(() => {
        (async () => {
            const { status } = await BarCodeScanner.requestPermissionsAsync();
            setHasPermission(status === 'granted');
        })();
    }, []);

    const handleBarCodeScanned = ({ type, data }) => {
        setCamera(false);
        dispatch(add({
            barcode: data
        }));
        console.log("barcode : " + data)
    };

    if (hasPermission === null) {
        return <Text>Requesting for camera permission</Text>;
    }

    if (hasPermission === false) {
        return <Text>No access to camera</Text>;
    }

    if (camera) {
        return <View style={styles.container}>
            <BarCodeScanner
                onBarCodeScanned={!camera ? undefined : handleBarCodeScanned}
                style={StyleSheet.absoluteFillObject}
            />
            <Button title="Close Camera" onPress={() => setCamera(false)}></Button>
        </View>;
    }

    return (
        <Layout style={styles.container}>
            <FlatList
                data={state.barcodes}
                renderItem={({ item }) => <Card style={styles.card}>
                    <Layout style={styles.layout}>
                        <Text>{item.id}</Text>
                        <Text>{item.barcode}</Text>
                    </Layout>
                </Card>}
            />
            <Button title="Scan Barcode" onPress={() => {
                setCamera(true);
            }} />
        </Layout>
    );
}

const styles = StyleSheet.create({
    container: {
        padding: 40,
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
    },
    card: {
        marginTop: 20,
    },
    layout: {
        alignSelf: 'stretch',
        flex: 1,
        flexDirection: "row",
        alignItems: 'center',
        justifyContent: 'space-between',
    }
});
