import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Button, FlatList, StyleSheet, TouchableOpacity, View } from 'react-native';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faHeart } from '@fortawesome/free-solid-svg-icons'
import { Layout, Text, Card } from '@ui-kitten/components';

import { add, remove } from '../store/actions/favoriteActions';

export default Receipts = () => {
    const favs = [
        {
            "id": 1,
            "name": "recipe",
            "barcode": "023023123",
            "product_id": 313
        },
        {
            "id": 2,
            "name": "recipe2",
            "barcode": "0233123",
            "product_id": 314
        },
        {
            "id": 3,
            "name": "recipe3",
            "barcode": "01234323123",
            "product_id": 315
        },
    ];
    const dispatch = useDispatch()
    const state = useSelector(state => state)

    return (
        <Layout style={styles.container}>
            <Button title={"Favories Count " + state.favorites.length} onPress={() => console.log("clicked")} />
            <FlatList
                data={favs}
                renderItem={({ item }) => (
                    <Card style={{ marginTop: 20 }}>
                        <Layout style={styles.card}>
                            <Text>{item.id}</Text>
                            <Text>{item.name}</Text>
                            <TouchableOpacity onPress={() => dispatch(add(item))}>
                                <View style={{ padding: 10 }}>
                                    <FontAwesomeIcon color="red" icon={faHeart} />
                                </View>
                            </TouchableOpacity>
                        </Layout>
                    </Card>
                )}
            ></FlatList>
        </Layout>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        padding: 60,
    },
    card: {
        alignSelf: 'stretch',
        flex: 1,
        flexDirection: "row",
        alignItems: 'center',
        justifyContent: 'space-between'
    }
});
