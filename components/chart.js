import React from 'react';
import { useState, useEffect } from 'react';
import { Button, StyleSheet, View, Dimensions } from 'react-native';
import {
    LineChart,
} from "react-native-chart-kit";
import { Layout, Text } from '@ui-kitten/components';
import axios from 'axios';
import { ThemeContext } from '../theme/theme-context';

export default Chart = () => {
    const themeContext = React.useContext(ThemeContext);
    const [label, setLabel] = useState([]);
    const [data, setData] = useState([])
    const [load, setLoad] = useState(false)

    function getUserPermissions() {
        return axios.get('http://142.93.180.33:8000/api/getChartApi')
            .then((res) => {
                console.log(res.data);
                if (res.data && res.data.layouts && res.data.data) {
                    setLabel(res.data.layouts)
                    setData(res.data.data.map(item => parseInt(item)))
                }
                setLoad(true);
            });
    }

    useEffect(() => {
        getUserPermissions();
    }, []);

    return (
        <Layout style={styles.container}>
            {load && <LineChart
                data={{
                    labels: label,
                    datasets: [
                        {
                            data: data
                        }
                    ]
                }}
                width={Dimensions.get("window").width - 120} // from react-native
                height={220}
                yAxisLabel="$"
                yAxisSuffix="k"
                yAxisInterval={1} // optional, defaults to 1
                chartConfig={{
                    backgroundColor: "#e26a00",
                    backgroundGradientFrom: "#fb8c00",
                    backgroundGradientTo: "#ffa726",
                    decimalPlaces: 2, // optional, defaults to 2dp
                    color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                    labelColor: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
                    style: {
                        borderRadius: 16
                    },
                    propsForDots: {
                        r: "6",
                        strokeWidth: "2",
                        stroke: "#ffa726"
                    }
                }}
                style={{
                    marginVertical: 8,
                    borderRadius: 16
                }}
            />}
            <Button title='TOGGLE THEME' onPress={themeContext.toggleTheme}></Button>
        </Layout>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        padding: 60,
    },
});
