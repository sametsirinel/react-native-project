import * as React from 'react';
import { Provider } from 'react-redux';
import { StyleSheet } from 'react-native';
import store from './store/store'
import Chart from './components/chart';
import Receipts from './components/receipts';
import Barcode from './components/barcode';
import Profile from './components/profile';
import * as eva from '@eva-design/eva';

import { ApplicationProvider } from '@ui-kitten/components';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { FontAwesomeIcon } from '@fortawesome/react-native-fontawesome'
import { faChartLine, faHeart, faBarcode, faUser } from '@fortawesome/free-solid-svg-icons'
const Tab = createBottomTabNavigator();
;
import { ThemeContext } from './theme/theme-context';

export default function App() {

  const [theme, setTheme] = React.useState('light');

  const toggleTheme = () => {
    const nextTheme = theme === 'light' ? 'dark' : 'light';
    setTheme(nextTheme);
  };

  return (
    <ThemeContext.Provider value={{ theme, toggleTheme }}>
      <ApplicationProvider {...eva} theme={eva[theme]}>
        <Provider store={store}>
          <NavigationContainer>
            <Tab.Navigator>
              <Tab.Screen options={{
                tabBarIcon: ({ color, size }) => (
                  <FontAwesomeIcon color={color} icon={faChartLine} />
                ),
              }} name="Charts" component={Chart} />
              <Tab.Screen options={{
                tabBarIcon: ({ color, size }) => (
                  <FontAwesomeIcon color={color} icon={faHeart} />
                ),
              }} name="Receipts" component={Receipts} />
              <Tab.Screen
                options={{
                  tabBarIcon: ({ color, size }) => (
                    <FontAwesomeIcon color={color} icon={faBarcode} />
                  ),
                }} name="Barcode" component={Barcode} />
              <Tab.Screen
                options={{
                  tabBarIcon: ({ color, size }) => (
                    <FontAwesomeIcon color={color} icon={faUser} />
                  ),
                }} name="Profile" component={Profile} />
            </Tab.Navigator>
          </NavigationContainer>
        </Provider>
      </ApplicationProvider>
    </ThemeContext.Provider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    justifyContent: 'center',
    padding: 60,
  },
});
